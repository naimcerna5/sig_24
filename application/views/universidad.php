<br>
<h1>DIRECCION DE LA UTC</h1>
<img src="<?php echo base_url('assets/img/utc.png');?>" alt="">
<br>
<div id="mapa1" style="width:100%; height:500px; border:10px solid black;">
</div>
<script type="text/javascript">
    function initMap(){
      var coordenadaCentral=new google.maps.LatLng(-0.9176428582274488, -78.63299861542403);

      var miMapa=new google.maps.Map(document.getElementById('mapa1'),
      {
        center: coordenadaCentral,
        zoom:9,
        mapTypeId: google.maps.MapTypeId.ROADDMAP
      }
    );
    //Instansear (LatLng)
    var marcadorUtc = new google.maps.Marker
    ({ position: new google.maps.LatLng(-0.917758614643168, -78.6330179920543),
    map: miMapa,
    title: 'UTC La Matriz'});
    var marcadorPujili = new google.maps.Marker({
    position: new google.maps.LatLng(-0.9581447343300331, -78.69648063438257),
    map: miMapa,
    title: 'UTC Campus Pujili',
    icon:'<?php echo base_url('assets/img/utc.png');?>  '
      });
    var marcadorSalache = new google.maps.Marker
    ({ position: new google.maps.LatLng(-0.9993684225470414, -78.61906897303174),
    map: miMapa,
    title: 'UTC Campus Salache'
      });
    var marcadorLaMana = new google.maps.Marker
      ({ position: new google.maps.LatLng(-0.9471414229974474, -79.22510043396728),
      map: miMapa,
      title: 'UTC Campus La Mana'
        });
    }
</script>
